package com.example.client_uts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_pendataan1.view.*

class PendataanAdapter(val pendataan1 : ArrayList<Pendataan>, val onClick : OnClick) : RecyclerView.Adapter<PendataanAdapter.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pendataan1, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int = pendataan1.size

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bind(pendataan1.get(position))

        holder.itemView.setOnClickListener {
            onClick.edit(pendataan1.get(position))
        }
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(pendataan : Pendataan){
            itemView.tvNik.text = pendataan.nik
            itemView.tvNamaLengkap.text = pendataan.nama
            itemView.tvAlamat.text = pendataan.alamat
        }
    }

    interface OnClick {
        fun delete(key : String?)
        fun edit(pendataan : Pendataan?)
    }
}